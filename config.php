<?php 

	define("FOLDER_PROYECTO", $_SERVER["CONTEXT_DOCUMENT_ROOT"] . "/proyecto/");
	define("FOLDER_TEMPLATE", FOLDER_PROYECTO . "template/");
	define("URL_PROYECTO", "/proyecto/");
	define("URL_CSS", URL_PROYECTO . "assets/css/");
	define("URL_FONTS", URL_PROYECTO . "assets/fonts/");
	define("URL_IMG", URL_PROYECTO . "assets/img/");
	define("URL_JS", URL_PROYECTO . "assets/js/");
	include FOLDER_PROYECTO . "app/func/session.php";
?>
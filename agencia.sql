-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-06-2019 a las 15:28:48
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agencia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `Id` int(11) NOT NULL,
  `Documento` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `Nombres` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `Apellidos` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `Direccion` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `Telefono` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0',
  `Email` varchar(200) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizaciones`
--

CREATE TABLE `cotizaciones` (
  `Id` int(11) NOT NULL,
  `IdDestino` int(11) NOT NULL DEFAULT '0',
  `IdCliente` int(11) NOT NULL DEFAULT '0',
  `Fecha_Cotizacion` date NOT NULL,
  `Fecha_Ida` date NOT NULL,
  `Fecha_Regreso` date NOT NULL,
  `Cantidad_Adultos` int(11) NOT NULL DEFAULT '0',
  `Cantidad_Ninos` int(11) NOT NULL DEFAULT '0',
  `Estado` tinyint(4) NOT NULL DEFAULT '0',
  `Valor` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinos`
--

CREATE TABLE `destinos` (
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Precio_Adulto` int(11) NOT NULL DEFAULT '0',
  `Precio_Nino` int(11) NOT NULL DEFAULT '0',
  `Activo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `destinos`
--

INSERT INTO `destinos` (`Id`, `Descripcion`, `Precio_Adulto`, `Precio_Nino`, `Activo`) VALUES
(107, 'Amazonas', 500, 300, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntosventa`
--

CREATE TABLE `puntosventa` (
  `Id` int(11) NOT NULL,
  `Descripcion` varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Direccion` varchar(200) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Telefono` varchar(20) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Activo` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `puntosventa`
--

INSERT INTO `puntosventa` (`Id`, `Descripcion`, `Direccion`, `Telefono`, `Activo`) VALUES
(2, 'nuevo punto', 'cr5 44.4', '32231', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservas`
--

CREATE TABLE `reservas` (
  `Id` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `IdCotizacion` int(11) NOT NULL DEFAULT '0',
  `IdUsuario` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `Id` smallint(5) NOT NULL,
  `Descripcion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `Activo` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`Id`, `Descripcion`, `Activo`) VALUES
(3, 'Asesor', b'1'),
(4, 'Administrador', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL,
  `Documento` varchar(20) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Nombres` varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Apellidos` varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Telefono` varchar(20) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Email` varchar(100) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Direccion` varchar(200) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Rol` smallint(5) NOT NULL DEFAULT '0',
  `Password` varchar(50) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `Activo` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id`, `Documento`, `Nombres`, `Apellidos`, `Telefono`, `Email`, `Direccion`, `Rol`, `Password`, `Activo`) VALUES
(11, '0102', 'Carlos', 'Rodriguez', '090909', 'aoccom.0@gmail.com', 'kr 45 #77-00', 3, '123', b'1'),
(12, '000', 'Admin', 'Admin', '98989', 'aoccom.0@gmail.com', 'kkjk', 4, '123', b'1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariospuntoventa`
--

CREATE TABLE `usuariospuntoventa` (
  `IdUsuario` int(11) NOT NULL DEFAULT '0',
  `IdPuntoVenta` int(11) NOT NULL DEFAULT '0',
  `Fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Destino` (`IdDestino`),
  ADD KEY `FK_Cliente` (`IdCliente`);

--
-- Indices de la tabla `destinos`
--
ALTER TABLE `destinos`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `puntosventa`
--
ALTER TABLE `puntosventa`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Cotizacion` (`IdCotizacion`),
  ADD KEY `FK_UsuarioRes` (`IdUsuario`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Rol` (`Rol`);

--
-- Indices de la tabla `usuariospuntoventa`
--
ALTER TABLE `usuariospuntoventa`
  ADD PRIMARY KEY (`IdUsuario`,`IdPuntoVenta`,`Fecha`),
  ADD KEY `FK_Punto` (`IdPuntoVenta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `destinos`
--
ALTER TABLE `destinos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT de la tabla `puntosventa`
--
ALTER TABLE `puntosventa`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reservas`
--
ALTER TABLE `reservas`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `Id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD CONSTRAINT `FK_Cliente` FOREIGN KEY (`IdCliente`) REFERENCES `clientes` (`Id`),
  ADD CONSTRAINT `FK_Destino` FOREIGN KEY (`IdDestino`) REFERENCES `destinos` (`Id`);

--
-- Filtros para la tabla `reservas`
--
ALTER TABLE `reservas`
  ADD CONSTRAINT `FK_Cotizacion` FOREIGN KEY (`IdCotizacion`) REFERENCES `cotizaciones` (`Id`),
  ADD CONSTRAINT `FK_UsuarioRes` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`Id`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `FK_Rol` FOREIGN KEY (`Rol`) REFERENCES `roles` (`Id`);

--
-- Filtros para la tabla `usuariospuntoventa`
--
ALTER TABLE `usuariospuntoventa`
  ADD CONSTRAINT `FK_Punto` FOREIGN KEY (`IdPuntoVenta`) REFERENCES `puntosventa` (`Id`),
  ADD CONSTRAINT `FK_Usuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuarios` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

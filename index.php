<?php include 'config.php'; ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include FOLDER_TEMPLATE . 'head.php'; ?>
<body>          
    <div id="wrapper">
        <?php include FOLDER_TEMPLATE . 'top.php'; ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>INICIO DE SESIÓN</h2>  
                     <div >
                         <form action="<?= URL_PROYECTO ?>app/login/login_controller.php" method="post" class="row">
                             <div class="col-md-6">
                                 <div class="form-group">
                                     <label for="txtDocumento">Documento</label>
                                     <input type="text" name="documento" placeholder="Document">
                                 </div>
                                 <div class="form-group">
                                     <label for="txtPass">Contraseña</label>
                                     <input type="password" name="pass" placeholder="Password" >
                                 </div>
                                 <div class="form-group">
                                     <input type="submit" name="btnIngresar" class="btn btn-success" value="Ingresar">
                                 </div>
                             </div>
                         </form>
                     </div> 
                    </div>
                </div>
                <hr />
            </div>
        </div>
    </div> 

    <?php include FOLDER_TEMPLATE . 'footer.php'; ?>
    <?php include FOLDER_TEMPLATE . 'scripts.php'; ?> 
</body>
</html>
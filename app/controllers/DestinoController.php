<?php

class DestinoController
{
    public function __construct()
    {
        require_once "../models/DestinoModel.php";
    }

    public function index()
    {
        $destino = new DestinoModel();
        $resDestinos = $destino->getDestinos();
        require_once "../views/destinosView.php";
    }
    public function guardarDestino()
    {
        if (isset($_POST['btnGuardar'])) {
            $descripcion = $_POST["txtDescripcion"];
            $precioAdulto = $_POST["txtPrecioAdulto"];
            $precioNino = $_POST["txtPrecioNino"];
            $checkB = $_POST["chkActivo"];
            $activo = (int) $checkB;
            $destino = new DestinoModel();
            $destino->createDestino($descripcion, $precioAdulto, $precioNino, $activo);
            require_once '../../config.php';
            $mailer = new MailModel();
            $email = $destino->ConsultarEmail();
            // $des = $resDestinos[0]["Email"];
            $des = "aoccom.0@gmail.com";
            echo $des;
            $mailer->enviarMail($des, $descripcion);

            header("location: DestinoController.php");
        }
    }

    public function editarDestino($id)
    {
        $destino = new DestinoModel();
        $dt = $destino->getDestino($id);
        require_once '../views/FrmEditDestinos.php';
    }

    public function mostrarFrmDestino()
    {
        require_once '../views/FrmDestinos.php';
    }

    public function actualizar()
    {
        if (isset($_POST["btnEditGuardar"])) {
            $id = $_POST['hId'];
            $descripcion = $_POST["txtDescripcion"];
            $precioAdulto = $_POST["txtPrecioAdulto"];
            $precioNino = $_POST["txtPrecioNino"];
            $checkB = $_POST["chkActivo"];
            $activo = (int) $checkB;
            $destino = new DestinoModel();
            $destino->updateDestino($id, $descripcion, $precioAdulto, $precioNino, $activo);
        }
        header("location: DestinoController.php");
    }

    public function eliminar($id)
    {
        $destino = new DestinoModel();
        $destino->deleteDestino($id);
        header("location: DestinoController.php");
    }
}

$contoller = new DestinoController();
if (isset($_POST["btnGuardar"])) {
    $contoller->guardarDestino();
} elseif (isset($_POST['btnInsertar'])) {
    $contoller->mostrarFrmDestino();
} else if (isset($_GET["edit"])) {
    $contoller->editarDestino($_GET["edit"]);
} else if (isset($_POST['btnEditGuardar'])) {
    $contoller->actualizar();
} else if (isset($_GET["borrar"])) {
    $contoller->eliminar($_GET["borrar"]);
} else {
    $contoller->index();
}

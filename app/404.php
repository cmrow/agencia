<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>No encontrado</title>
</head>
<body>

</body>
</html>

<?php 
    require_once '../config.php'; 
    validar_sesion();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include FOLDER_TEMPLATE . 'head.php'; ?>
<body>          
    <div id="wrapper">
        <?php include FOLDER_TEMPLATE . 'top.php'; ?>
        <?php include FOLDER_TEMPLATE . 'menu.php'; ?>
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                	<div style="margin: auto;">
						<img src="../assets/img/404.jpg" >
					</div>
                </div>
                <hr />
            </div>
        </div>
    </div> 

    <?php include FOLDER_TEMPLATE . 'footer.php'; ?>
    <?php include FOLDER_TEMPLATE . 'scripts.php'; ?> 
</body>
</html>
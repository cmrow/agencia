<?php
require_once '../../config.php';
validar_sesion();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include FOLDER_TEMPLATE . 'head.php'; ?>

<body>
    <div id="wrapper">
        <?php include FOLDER_TEMPLATE . 'top.php'; ?>
        <?php include FOLDER_TEMPLATE . 'menu.php'; ?>
        <div id="page-wrapper">
            <div id="page-inner">

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <div style="padding:20px; border:solid; margin:10px"">
                        <h3>Formularios Destinos</h3><br>
                            <form action=" DestinoController.php" method="post">
                            <div class="row">
                                <div class="col-md-3">
                                    <label> Destino</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" name="txtDescripcion" class="form-control col-md-6">
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-md-3">
                                    <label> Precio adulto</label>
                                </div>
                                <div class="col-md-8 input-group" style="padding:14px">
                                    <span class="input-group-addon">$</span>
                                    <input type="number" name="txtPrecioAdulto" class="form-control">
                                </div> <br>
                                <div class="col-md-3">
                                    <label> Precio Niños</label>
                                </div>
                                <div class="col-md-8 input-group" style="padding:14px">
                                    <span class="input-group-addon">$</span>
                                    <input type="number" name="txtPrecioNino" class="form-control">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Estado</label>
                                </div>
                                <div class="col-md-3">
                                    <select name="chkActivo" id="">
                                        <option selected> - Seleccione - </option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input style="width: 100%;" type="submit" name="btnReset" value="Cancelar" class="btn btn-warning">
                                </div>
                                <div class="col-md-3">
                                    <input style="width: 100%" type="submit" name="btnGuardar" value="GUARDAR" class="btn btn-success">
                                </div>

                            </div>
                            <div class="row mt-2">


                            </div><br>



                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr />
    </div>
    </div>
    </div>

    <?php include FOLDER_TEMPLATE . 'footer.php'; ?>
    <?php include FOLDER_TEMPLATE . 'scripts.php'; ?>

    <script>
        function eliminar(id) {
            if (confirm("¿Desea eliminar este punto de venta?")) {
                document.location.href = "puntosventa_controller.php?delid=" + id;
            }
        }
    </script>
</body>

</html>
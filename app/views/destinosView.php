<?php
require_once '../../config.php';
validar_sesion();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include FOLDER_TEMPLATE . 'head.php'; ?>

<body>
    <div id="wrapper">
        <?php include FOLDER_TEMPLATE . 'top.php'; ?>
        <?php include FOLDER_TEMPLATE . 'menu.php'; ?>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <form action="DestinoController.php" method="POST">
                            <h1>Destinos Nacionales e Internacionales</h1>

                            <div class="table-responsive mt-1">
                                <table class="table" style="padding:20px; border:solid; margin:10px">
                                    <thead>
                                        <tr>
                                            <th>Destino</th>
                                            <th>$ Precio Adultos </th>
                                            <th>$ Precio Niños</th>
                                            <th>Estado</th>
                                            <th>Editar</th>
                                            <th>Eliminar</th>
                                        </tr>
                                        <input type="submit" name="btnInsertar" value="NUEVO DESTINO" class="btn btn-success">
                                        <!-- <tr><input type="submit" name="btnInsertar" value="Nuevo Destino" class="btn btn-success"><br></tr> -->
                                        <tr>&nbsp;</tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $controller = new DestinoController();
                                        foreach ($resDestinos as $item) {
                                            echo "<tr>";
                                            echo "<td>" . $item["Descripcion"] . "</td>";
                                            echo "<td>" . $item["Precio_Adulto"] . "</td>";
                                            echo "<td>" . $item["Precio_Nino"] . "</td>";
                                            echo "<td>";

                                            if ($item["Activo"] == 1) {
                                                echo "Activo";
                                            } else {
                                                echo "Inactivo";
                                            }
                                            echo "</td>";
                                            echo "<td><a href='DestinoController.php?edit=" . $item["Id"] . "'><input type='button' name='btnActualizar' value='Editar' class='btn btn-primary '></a></td>";
                                            echo "<td><input type='button' name='btnEliminar'  value='Eliminar' class='btn btn-danger' onclick='eliminar(" . $item["Id"] . ")'></td>";
                                        }

                                        ?>
                                    </tbody>
                                </table>
                        </form>
                    </div>

                </div>

            </div>
        </div>
        <hr />
    </div>
    </div>
    </div>

    <?php include FOLDER_TEMPLATE . 'footer.php'; ?>
    <?php include FOLDER_TEMPLATE . 'scripts.php'; ?>

    <script>
        function eliminar(id) {
            if (confirm("¿Desea eliminar este punto de venta?")) {
                document.location.href = "DestinoController.php?borrar=" + id;
            }
        }
    </script>
</body>

</html>
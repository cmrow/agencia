<?php
require_once '../../config.php';
validar_sesion();
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include FOLDER_TEMPLATE . 'head.php'; ?>

<body>
    <div id="wrapper">
        <?php include FOLDER_TEMPLATE . 'top.php'; ?>
        <?php include FOLDER_TEMPLATE . 'menu.php'; ?>
        <div id="page-wrapper">
            <div id="page-inner">

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <div style="padding:20px; border:solid; margin:10px">
                        <h3>Formularios Destinos</h3><br>
                            <form action=" DestinoController.php" method="post">
                            <?php
                            $controller = new DestinoController();
                            
                            // echo "<input type='hidden' name='hId'> value='" . $dt[0]["Id"] . "'";
                            
                            echo "<div class='row'>";
                            echo "<div class='col-md-3'>";
                            echo "<label> Destino</label>";
                            echo "</div>";
                            echo "<div class='col-md-8'>";
                            echo "<input type='text' value='" . $dt[0]["Descripcion"] . "' name='txtDescripcion' class='form-control col-md-6'>";
                            echo "</div>";
                            echo "</div><br>";
                            echo "<div class='row'>";
                            echo "<div class='col-md-3'>";
                            echo "<label> Precio adulto</label>";
                            echo "</div>";
                            echo "<div class='col-md-8 input-group' style='padding:14px'>";
                            echo "<span class='input-group-addon'>$</span>";
                            echo "<input type='number' value='" . $dt[0]["Precio_Adulto"] . "' name='txtPrecioAdulto' class='form-control'>";
                            echo "</div> <br>";
                            echo "<div class='col-md-3'>";
                            echo "<label> Precio Niños</label>";
                            echo "</div>";
                            echo "<div class='col-md-8 input-group' style='padding:14px'>";
                            echo "<span class='input-group-addon'>$</span>";
                            echo "<input type='number' value='" . $dt[0]["Precio_Nino"] . "' name='txtPrecioNino' class='form-control'>";
                            echo "</div>";
                            echo "</div>";
                            echo "<br>";
                            echo "<div class='row'>";
                            echo "<div class='col-md-3'";
                            echo "<label>Estado</label>";
                            echo "</div>";
                            $act = ($dt[0]["Activo"] == 1) ? 'selected' : "";
                            $inact = ($dt[0]["Activo"] == 0) ? 'selected' : "";

                            echo "<div class='col-md-3'>";
                            echo "<select name='chkActivo'>";
                            echo "<option value='1' " . $act . " >Activo</option>";
                            echo "<option value='0' " . $inact . " >Inactivo</option>";
                            echo "</select>";
                            
                            echo "</div>";
                            echo "<input type='hidden' name='hId' value='" . $dt[0]["Id"] . "'>";
                            ?>


                            <div class="col-md-3">
                                <input style="width: 100%;" type="submit" name="btnReset" value="Cancelar" class="btn btn-warning">
                            </div>
                            <div class="col-md-3">
                                <input style="width: 100%" type="submit" name="btnEditGuardar" value="GUARDAR" class="btn btn-success">
                            </div>

                        </div>
                      



                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr />
    </div>
    </div>
    </div>

    <?php include FOLDER_TEMPLATE . 'footer.php'; ?>
    <?php include FOLDER_TEMPLATE . 'scripts.php'; ?>

    <script>
        function eliminar(id) {
            if (confirm("¿Desea eliminar este punto de venta?")) {
                document.location.href = "puntosventa_controller.php?delid=" + id;
            }
        }
    </script>
</body>

</html>
<?php 
	session_start();

	function sesion_activa() {
		return isset($_SESSION["usuario"]);
	}

	function validar_sesion() {
		if (!sesion_activa()) {
			header("location: " . URL_PROYECTO);
		}
	}
?>
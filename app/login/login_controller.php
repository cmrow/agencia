<?php 

	require_once '../../config.php';
	require_once '../models/conexion.php';

	if (isset($_POST["documento"], $_POST["pass"])) {
		$documento = $_POST["documento"];
		$pass = $_POST["pass"];

		$sql = "select * from usuarios where documento = ? and password = ?";

		$conn = conexion::conectar();
		$st = $conn->prepare($sql);
		$st->execute([$documento, $pass]);

		if ($st->fetchColumn() > 0) {
			$_SESSION["usuario"] = $documento;
			header("location: " . URL_PROYECTO . "app/index.php");
		} else {
			header("location: ". URL_PROYECTO);
		}
	} else {
		header("location: ". URL_PROYECTO);
	}

?>
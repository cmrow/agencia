<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once('phpMailer/Exception.php');
require_once('phpMailer/PHPMailer.php');
require_once('phpMailer/SMTP.php');

class MailModel extends PHPMailer
{
    private $bd;
    private $destinatario;
    public function __construct()
    {
        // require_once("model/Conexionn.php");
        // $this->bd = Conexionn::conexion();

        try {
            //Server settings
            $this->SMTPDebug = 1;                                       // Enable verbose debug output
            $this->isSMTP();                                            // Set mailer to use SMTP
            $this->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
            $this->SMTPAuth   = true;                                   // Enable SMTP authentication
            $this->Username   = 'agenciaphp@gmail.com';                     // SMTP username
            $this->Password   = 'clasephp123';                               // SMTP password
            $this->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $this->Port       = 587;                                    // TCP port to connect to
            $this->setFrom('agenciaphp@gmail.com', 'AgenciaMailer');
            // $this->addAddress("aoccom.0@gmail.com", "Carlos");
            // $this->Subject = 'Prueba';
            // $this->Body    = '<h3>This is the HTML</h3> message body <b>in bold!</b>';
            // $this->AltBody = 'This is the body in plain text for non-HTML mail clients';

            //Recipients
            // $this->addAddress($destinatario, 'Joe User');     // Add a recipient

            $this->isHTML(true);                                  // Set email format to HTML
            // $this->send();
            // $this->Subject = 'Here is the subject';
            // $this->AltBody = 'This is the body in plain text for non-HTML mail clients';
            // $this->send();
            // echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error model: {$this->ErrorInfo}" . $e;
        }
    }

    function enviarMail($des, $body)
    {
        $mailer = new MailModel();
        $mailer->addAddress($des, "Carlos");
        $mailer->Subject = 'Crear destino de viajes';
        $mailer->AltBody = 'HTML mail clientes';
        $mailer->Body    = $body;
        // $mailer->configMail();
        $flag = $mailer->send();

        if ($flag) {
            echo 'Message has been sent';
            $mailer->addAddress(" ", "");
        } else {
            echo 'Message could not be sent. Mailer Error Pila';
        }
    }
}

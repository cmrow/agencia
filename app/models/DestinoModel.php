<?php

class DestinoModel
{
    private $bd;
    private $destinos;
    
    public function __construct()
    {
        require_once 'MailModel.php';
        require_once 'conexion.php';
        $this->bd = conexion::conectar();
        $this->destinos = array();
    }

    public function getDestinos()
    {
        $sql = "SELECT * FROM destinos";
        $st = $this->bd->query($sql);
        $this->destinos = $st->fetchAll(PDO::FETCH_ASSOC);
        return $this->destinos;
    }

    public function getDestino($id)
    {
        $sql = "SELECT * FROM destinos WHERE Id = $id";
        $st = $this->bd->query($sql);
        $this->destinos = $st->fetchAll(PDO::FETCH_ASSOC);
        return $this->destinos;
    }

    public function createDestino($descripcion, $precio_adulto, $precio_nino, $activo)
    {
        $sql = "INSERT INTO destinos (`Descripcion`,`Precio_Adulto`,`Precio_Nino`,`Activo`) 
        VALUES (?,?,?,?)";
        $st = $this->bd->prepare($sql);
        $st->execute([$descripcion, $precio_adulto, $precio_nino, $activo]);


    }

    public function updateDestino($id, $descripcion, $precio_adulto, $precio_nino, $activo)
    {
        $sql = "UPDATE agencia.destinos SET `Descripcion` = ?, `Precio_Adulto` = ?, `Precio_Nino` = ?, `Activo` = ? WHERE `Id` = ?";
        $st = $this->bd->prepare($sql);
        $st->execute([$descripcion, $precio_adulto, $precio_nino, $activo, $id]);
    }

    public function deleteDestino($id)
    {
        $sql = "delete from destinos where Id = $id";
        $st = $this->bd->query($sql);
        $st->execute([$id]);

    }

    public function ConsultarEmail() {
        $sql = "SELECT Email FROM usuarios WHERE Nombres = 'Admin'";
        $st = $this->bd->query($sql);
        $this->email = $st->fetchAll(PDO::FETCH_ASSOC);
        return $this->email;
    }
}
